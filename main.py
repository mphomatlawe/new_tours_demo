from selenium import webdriver

driver = webdriver.Chrome('C:\\Users\\mmatlawe\\PycharmProjects\\NewTours_Demo\\Drivers\\chromedriver.exe')

driver.get("http://demo.guru99.com/test/newtours/")

driver.find_element_by_xpath("//a[contains(.,'SIGN-ON')]").click()
driver.find_element_by_xpath("//input[@name='userName']").send_keys("Test")
driver.find_element_by_xpath("//input[@name='password']").send_keys("Test")
driver.find_element_by_xpath("//input[@name='submit']").click()

login_successful = driver.find_element_by_xpath("//h3[contains(.,'Login Successfully')]").text

if login_successful == "Login Successfully":
    print("Login Passed")
    driver.close()
    assert True

else:
    print("Login failed, user unable to see thank you for loggin: " + login_successful)
    driver.close()
    assert False
